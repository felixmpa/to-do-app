# Creating a Simple Todo App with AngularJS and C# MVC #

A simple Todo List Application using (AngularJS, C# MVC 4 and SQL-Server).

* Single page application to create and delete tasks.
* Creating a RESTful  API Controller with C#.
* Using AngularJS for the frontend and to access the API.
* Storing data in a database using SQL Server R2.

### How do I get set up? ###

* Visual Studio C# MVC Project
* Dependencies: Angular JS, Underscore, Bootstrap
* Database configuration
* How to run tests and deployment instructions: blog.felixmpa.info