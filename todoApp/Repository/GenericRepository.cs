﻿using todoApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace todoApp.Repository
{
    public class GenericRepository<TEntity> where TEntity : class
    {
        private TodoAppConnection context;
        private DbSet<TEntity> dbset;

        public GenericRepository(TodoAppConnection context)
        {
            this.context = context;
            dbset = context.Set<TEntity>();
        }
      
        public TEntity GetById(int id)
        {
            return dbset.Find(id);
        }

        public List<TEntity> GetAll()
        {
            return dbset.ToList();
        }

        public bool Create(TEntity entity)
        {
           dbset.Add(entity);
           return (context.SaveChanges() > 0);
        } 

        public bool Edit(TEntity entity)
        {
            context.Entry(entity).State = EntityState.Modified;
            return (context.SaveChanges() > 0);
        }

        public bool Delete(TEntity entity)
        {
            context.Entry(entity).State = EntityState.Deleted;
            return (context.SaveChanges() > 0);
        }
    }
}