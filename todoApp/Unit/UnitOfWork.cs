﻿using System;
using todoApp.Models;
using todoApp.Repository;

namespace todoApp.Unit
{
    public class UnitOfWork : IDisposable
    {
        private TodoAppConnection context = new TodoAppConnection();
        private GenericRepository<Todo> todoRepository;

        public GenericRepository<Todo> TodoRepository
        {
            get
            {

                if (this.todoRepository == null)
                {
                    this.todoRepository = new GenericRepository<Todo>(context);
                }
                return todoRepository;
            }
        }

        public void Save()
        {
            context.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}