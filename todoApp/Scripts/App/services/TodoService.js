﻿app.service('TodoService', ['$http', function($http) {

    //get data
    this.get = function () {

        return $http.get("http://localhost:49458/Todo/GetTodos").success(function (data) {
            return data;
        }).error(function (err) {
            return err;
        });

    };

    //post data
    this.create = function (todoObj) {

        var config = {
            method: "POST",
            url: "http://localhost:49458/Todo/CreateTodo",
            data: todoObj
        };

        return $http(config).success(function (data) {
            return data;
        }).error(function (err) { return err; })
    };


    //edit data
    this.edit = function (todoObj) {

        var todo = {
            "id": todoObj.id,
            "task": todoObj.task,
            "date": todoObj.date,
            "done": todoObj.done
        }

        var config = {
            method: "POST",
            url: "http://localhost:49458/Todo/EditTodo",
            data: todo
        };

        return $http(config).success(function (data) {
            return data;
        }).error(function (err) { return err; })
    };


    this.delete = function (todoObj) {
        var config = {
            method: "POST",
            url: "http://localhost:49458/Todo/DeleteTodo",
            data: todoObj
        };

        return $http(config).success(function (data) {
            return data;
        }).error(function (err) { return err; })
    }

}]);