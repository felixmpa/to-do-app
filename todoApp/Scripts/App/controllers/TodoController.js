﻿app.controller('TodoController', ['$scope', 'TodoService', function($scope, TodoService){

    //return all tasks    
    TodoService.get().success(function (data) {
        $scope.todos = data;
    });

    $scope.createTodo = function () {
        //if input empty return none
        if (!$scope.task || $scope.task == '') {
            return;
        }
        //create a new array with data-todo
        todo = {
            task: $scope.task,
            date: new Date(),
            done: 0
        };
        //send array to services post
        TodoService.create(todo).success(function (data) {
            $scope.todos.push(todo);
        });
        //clear the input-add
        $scope.task = '';
    };

    $scope.doneTodo = function (todo) {
        //mark done from list checkout
        (todo.done == 0 ? todo.done = 1 : todo.done = 0);
        TodoService.edit(todo).success(function (data) {
        });
    };

    $scope.clearTodo = function () {
        //clear task completed - using underscore to delete todo.done and make a clear array $.todos
        $scope.todos = _.filter($scope.todos, function (todo) {
            if(todo.done){  TodoService.delete(todo) } 
            return !todo.done;
        });
    };

    //editing current todo 
    $scope.renameTodo = function (todo) {
        todo['editing'] = true;
    };

    $scope.editTodo = function (todo) {
        //if input empty return none
        if (!$scope.task || $scope.task == '') {
            return;
        }
        //send array to services post
        $scope.todos = _.filter($scope.todos, function (todo) {
            if (todo.editing) {
                TodoService.edit(todo).success(function (data) {
                    return !todo.done;
                });
                $scope.todos.push($scope.task);
            }
        });

        //clear the input-add
        $scope.task = '';
    };
    
}]);
