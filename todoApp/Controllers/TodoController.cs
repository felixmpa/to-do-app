﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using todoApp.Unit;
using todoApp.Models;
using CodeCarvings.Piczard;

namespace todoApp.Controllers
{
    public class TodoController : Controller
    {

        public ActionResult Index()
        {
            return View();
        }

        UnitOfWork unit = new UnitOfWork();
        
        [HttpGet]
        public JsonResult GetTodos()
        {
            return Json(unit.TodoRepository.GetAll(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult CreateTodo(Todo todo)
        {
            return Json(unit.TodoRepository.Create(todo));
        }

        [HttpPost]
        public JsonResult EditTodo(Todo todo)
        {
            return Json(unit.TodoRepository.Edit(todo));
        }

        [HttpPost]
        public JsonResult DeleteTodo(Todo todo)
        {
            return Json(unit.TodoRepository.Delete(todo));
        }


        
     
    }
}